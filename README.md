# Trackr App
----------
Aplicacion movil basado en Cordova e Ionic para proyecto de inf del soft 2

## Desarrollo
Desde consola, teniendo instalado nodejs con npm ejecutas:

- Dependencias Globales:
```
$ npm install -g bower gulp stylus cordova ionic
```

- Instalar dependencias del proyectos:
```
$ npm install 
$ bower install 
```

## Ionic Tasks

- Testing LiveReload server
> Levanta servidor nodejs con un preview de la aplicacion
```
$ ionic serve
```

- Agregar plataforma
> Agrega plataforma para compilar con cordova 
```
$ ionic platform add android
```

- Agregar cordova plugins
> Agregar los plugins usados en la app
```
$ cordova plugin add cordova-plugin-inappbrowser
$ cordova plugin add https://github.com/apache/cordova-plugin-whitelist
```

- Compilar y correr app
> Compila y empaqueta aplicacion basado en las plataformas configuradas.
```
$ ionic build
$ ionic run
```